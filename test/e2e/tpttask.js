var config = require('../../nightwatch.conf.js');

module.exports = {
    before : function (browser) {
        browser.resizeWindow(800, 600);
      },
    'tpttask': function(browser) {
        browser
            .url('https://www.tptlive.ee/')
            .waitForElementPresent('body', 1000)
            .saveScreenshot(config.imgpath(browser) + 'tptlivemain.png')
            .click('li[id="menu-item-1313"]')
            .pause(3000)
            .saveScreenshot(config.imgpath(browser) + 'tunniplaan.png')
            .click('a[href="https://tpt.siseveeb.ee/veebivormid/tunniplaan/tunniplaan?oppegrupp=226&amp"]')
            .pause(3000)
            .saveScreenshot(config.imgpath(browser) + 'ta-17etunniplaan.png')
            .end();
    }
};

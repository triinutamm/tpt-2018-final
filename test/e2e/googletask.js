var config = require('../../nightwatch.conf.js');

module.exports = {
    'googletask': function(browser) {
        browser
            .url('http://google.com')
            .waitForElementPresent('body', 1000);

        browser.setValue('input[type=text]', ['tallinn', browser.Keys.ENTER])
            .pause(1000)
            .assert.containsText('#main', 'tallinn')
            .saveScreenshot(config.imgpath(browser) + 'google.png')
            .click('h3.LC20lb:nth-of-type(1)')
            .pause(1000)
            .saveScreenshot(config.imgpath(browser) + 'firstresult.png')
            .end();
    }
};



